from airflow import DAG
from datetime import datetime, timedelta
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.mysql_operator import MySqlOperator
from airflow.operators.email_operator import EmailOperator

from airflow.contrib.sensors.file_sensor import FileSensor
from datacleaner import data_cleaner

default_args = {
    'owner': 'Shravan',
    'start_date': datetime(2021, 6, 12),
    'retries': 1,
    'retry_delay': timedelta(seconds=5)
}

yesterday_date = datetime.strftime(datetime.now() - timedelta(1), '%Y-%m-%d')

with DAG(dag_id='store_dag_file_sensor', 
	     default_args = default_args,
	     schedule_interval = '@daily',
	     template_searchpath=['/usr/local/airflow/sql_files'],
	     catchup = False) as dag:

	# t1 = BashOperator(task_id='check_file_exists', 
	#               bash_command='shasum ~/store_files_airflow/raw_store_transactions.csv',
	#               retries=2,
	#               retry_delay = timedelta(seconds=15))

	t1 = FileSensor(
		task_id = 'check_file_sensor', 
		filepath = '/usr/local/airflow/store_files_airflow/raw_store_transactions.csv',
		fs_conn_id='fs_default',
		poke_interval = 10,
		timeout = 150,
		soft_fail = True)

	t2 = PythonOperator(task_id='clean_raw_csv', python_callable=data_cleaner)

	t3 = MySqlOperator(task_id='create_mysql_table', mysql_conn_id='mysql_conn_mine_new', sql='create_table.sql')

	t4 = MySqlOperator(task_id='insert_into_table', mysql_conn_id='mysql_conn_mine_new', sql='insert_into_table.sql')

	t5 = MySqlOperator(task_id='select_from_table', mysql_conn_id='mysql_conn_mine_new', sql='select_from_table.sql')

	t6 = BashOperator(task_id='move_file1', 
	              bash_command='cat ~/store_files_airflow/location_wise_profit.csv && mv ~/store_files_airflow/location_wise_profit.csv ~/store_files_airflow/location_wise_profit_%s.csv' % yesterday_date)

	t7 = BashOperator(task_id='move_file2', 
	              bash_command='cat ~/store_files_airflow/store_wise_profit.csv && mv ~/store_files_airflow/store_wise_profit.csv ~/store_files_airflow/store_wise_profit_%s.csv' % yesterday_date)

	t8 = EmailOperator(task_id='send_email',
        to='shravands69@gmail.com',
        subject='Daily report generated for stores',
        html_content=""" <h1>Congratulations! Your store reports are ready.</h1> """,
        files=['/usr/local/airflow/store_files_airflow/location_wise_profit_%s.csv' % yesterday_date, 
        '/usr/local/airflow/store_files_airflow/store_wise_profit_%s.csv' % yesterday_date]
        )

	t9 = BashOperator(trigger_rule = 'all_done', task_id='rename_raw', bash_command='mv ~/store_files_airflow/raw_store_transactions.csv ~/store_files_airflow/raw_store_transactions_%s.csv' % yesterday_date)

	t10 = BashOperator(trigger_rule = 'all_done', task_id='rename_raw_cleaned', bash_command='mv ~/store_files_airflow/clean_store_transactions.csv ~/store_files_airflow/clean_store_transactions%s.csv' % yesterday_date)


	t1 >> t2 >> t3 >> t4 >> t5 >> [t6, t7] >> t8 >> [t9, t10]
