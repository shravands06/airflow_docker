# this imports the data from s3 and process the data
# https://github.com/soumilshah1995/Airflow-Tutorials-/blob/main/Tutorial4/docker-compose.yml

from __future__ import print_function

import time
from builtins import range
from pprint import pprint

import airflow
from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator

# imports for file reading and data analyis
import pandas as pd

args = {'owner': 'Shravan',
        'start_date': airflow.utils.dates.days_ago(2)}

dag = DAG(dag_id='s3_dag', default_args=args,
          schedule_interval=None)

# file reading function

def file_read(ds, **kwargs):
	df = pd.read_csv('https://bucketstoredata.s3.ap-south-1.amazonaws.com/tips.csv')
	for i in list(df):
		print(i)
	sunday_data = df[df['day'] == 'Sun']
	sunday_data.to_csv('sunday_data.csv')
	return 'the file has been opened and executed'


file_read = PythonOperator(task_id='file_read',
                            provide_context=True,
                            python_callable=file_read, dag=dag)


# creation of the transformed file
def second_print(ds, **kwargs):
    print('This is the print of the second function')
    return 'This is the return of second function'


second_task = PythonOperator(task_id='second_function',
                             provide_context=True,
                             python_callable=second_print, dag=dag)

file_read >> second_task