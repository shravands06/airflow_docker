# the amin of this dag is to read and write files from s3

from __future__ import print_function

import time
from builtins import range
from pprint import pprint

import airflow
from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator

# custom imports
# import boto3
import pandas as pd
from io import BytesIO # python3; python2: BytesIO 
import time


args = {'owner': 'Shravan',
        'start_date': airflow.utils.dates.days_ago(2)}

dag = DAG(dag_id='s3_read_write', default_args=args,
          schedule_interval=None)
# getting the data file from s3
# s3 = boto3.client('s3')
# obj = s3.get_object(Bucket='bucketstoredata',Key='tips.csv')

# file reading function

current_time = lambda: int(round(time.time()))
output_file_name = 'df_export'+ str(current_time()) + '.csv'

def file_read(ds, **kwargs):
	print("the read function has been initiated")
	df = pd.read_csv(obj['Body'])
	#df = pd.read_csv('https://bucketstoredata.s3.ap-south-1.amazonaws.com/tips.csv')
	for i in list(df):
		print(i)
	sunday_data = df[df['day'] == 'Sun']
	#sunday_data.to_csv('https://bucketstoredata.s3.ap-south-1.amazonaws.com/sunday_data.csv')
	csv_buffer = BytesIO()
	sunday_data.to_csv(csv_buffer)
	# s3_resource = boto3.resource('s3')
	# s3_resource.Object('bucketstoredata', output_file_name ).put(Body=csv_buffer.getvalue())

	return 'the file has been opened and executed'


file_read = PythonOperator(task_id='file_read',
                            provide_context=True,
                            python_callable=file_read, dag=dag)


# creation of the transformed file
def second_print(ds, **kwargs):
    print('This is the print of the second function')
    return 'This is the return of second function'


second_task = PythonOperator(task_id='second_function',
                             provide_context=True,
                             python_callable=second_print, dag=dag)

file_read >> second_task