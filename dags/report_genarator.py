from __future__ import print_function

import time
import os
from builtins import range
from pprint import pprint
from datetime import timedelta, datetime

import airflow
from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.email_operator import EmailOperator

# requiremnt specific modules

import pandas as pd 
import matplotlib.pyplot as plt

AIRFLOW_HOME = os.getenv('AIRFLOW_HOME')
file_path = AIRFLOW_HOME + '/dags/tips.csv'

def loadCsv(**kwargs):
    df = pd.read_csv(file_path)
    print(list(df))
    df_day_total = df.groupby('day').sum()
    print('the calculation has been done')
    plt.bar(x = df_day_total.index, height= df_day_total['total_bill'])
    plt.xlabel('The days value')
    plt.ylabel('Sum of amount')
    save_path = AIRFLOW_HOME + '/dags/data_files/output/tips_day_sum.pdf'
    plt.savefig(save_path)
    print('the report has been generated')

def printName(**kwargs):
    print("this is the print of the second function")
    return('the return of the second function')
    
with DAG(
        dag_id="report_genarator",
        schedule_interval="@daily",
        tags=['report', 'plot', 'pandas'],
        default_args={
            "owner": "Shravan",
            "retries": 1,
            "start_date": datetime(2021, 1, 1),
        },
        catchup=False) as f:
  
    first_task = PythonOperator(
        task_id = 'reading_file',
        python_callable = loadCsv,
        email_on_failure=True,
        email='shravands69@gmail.com',
        provide_context=True)

    second_task = PythonOperator(
        task_id = 'simple_print',
        python_callable = printName,
        provide_context=True)

    email = EmailOperator(
        mime_charset='utf-8',
        task_id='send_email',
        to='shravands69@gmail.com',
        subject='Airflow Alert',
        html_content="Trying to send an email from airflow through SES."
    )


first_task >> second_task >> email
