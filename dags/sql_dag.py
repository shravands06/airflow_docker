# sql writing dag

from __future__ import print_function

import time
from builtins import range
from pprint import pprint

import airflow
from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.hooks.mysql_hook import MySqlHook

args = {
    'owner': 'Shravan',
    'start_date': airflow.utils.dates.days_ago(2),
}

dag = DAG(
    dag_id='sql_writing_dag',
    default_args=args,
    schedule_interval=None,
)

def selecting_mysql(**kwargs):
    connection = MySqlHook(mysql_conn_id='mysql_write')
    sql = 'select * from orders LIMIT 10'
    cursor = connection.cursor()
    cursor.execute(sql)
    data = cursor.fetchall()
    for i in data:
    	print(data[1])
    print("The data has been written to db")

sql_read = PythonOperator(task_id='sql_read',
                            provide_context=True,
                            python_callable=selecting_mysql, dag=dag)

def second_print(ds, **kwargs):
    print('This is the print of the second function')
    return 'This is the return of second function'


second_task = PythonOperator(task_id='second_function',
                             provide_context=True,
                             python_callable=second_print, dag=dag)

sql_read >> second_task
    