# This is the sample dag used for scrapping

from __future__ import print_function

import time
from builtins import range
from pprint import pprint

import airflow
from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator

# imports for scrapping

import urllib.request
import urllib.parse
import urllib.error
from bs4 import BeautifulSoup
import ssl

args = {'owner': 'Shravan',
        'start_date': airflow.utils.dates.days_ago(2)}

dag = DAG(dag_id='scrapper_dag', default_args=args,
          schedule_interval=None)

# Ignore SSL certificate errors

ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

# url used for scrrapping

url = \
    'https://www.hymedskincare.com/product-category/toners-exfoliants-masks/'

html = urllib.request.urlopen(url).read()
soup = BeautifulSoup(html, 'html.parser')
productname = []


# scrapping function

def scrapping_function(ds, **kwargs):

    pprint('The scrapping function has been initiated')
    for product_name in soup.find_all('h2',
            {'class': 'woocommerce-loop-product__title'}):
        print(product_name.text)
        productname.append(product_name.text)

    return 'The scrapping  has been done'


first_task = PythonOperator(task_id='scrapping_function',
                            provide_context=True,
                            python_callable=scrapping_function, dag=dag)


def second_print(ds, **kwargs):
    print('This is the print of the second function')
    return 'This is the return of second function'


second_task = PythonOperator(task_id='second_function',
                             provide_context=True,
                             python_callable=second_print, dag=dag)

first_task >> second_task
