# This is the sample file created 

from __future__ import print_function

import time
from builtins import range
from pprint import pprint

import airflow
from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.hooks.mysql_hook import MySqlHook

args = {
    'owner': 'Shravan',
    'start_date': airflow.utils.dates.days_ago(2),
}

dag = DAG(
    dag_id='mysql_sqmple',
    default_args=args,
    schedule_interval=None,
)


# [START howto_operator_python]
def print_context(ds, **kwargs):
    pprint("This is pprint of first function")
    print("This is print of first function this line added from cmd ")
    print(ds)
    print(kwargs)
    return 'This is the return of first function'


first_task = PythonOperator(
    task_id='first_function',
    provide_context=True,
    python_callable=print_context,
    dag=dag,
)
# [END howto_operator_python]


def second_print(ds, **kwargs):
    print("This is the print of the second function")
    return 'This is the return of second function'


second_task = PythonOperator(
    task_id='second_function',
    provide_context=True,
    python_callable=second_print,
    dag=dag,
)


def writing_mysql(**kwargs):
    connection = MySqlHook(mysql_conn_id='my_db_airflow')
    sql = 'INSERT INTO users(name) VALUES ("shravands")'
    connection.run(sql, autocommit=True, parameters=(name))
    print("The data has been written to db")
    


sql_writing = PythonOperator(
    task_id='mysql_writing',
    provide_context=True,
    python_callable=writing_mysql,
    dag=dag,
)

first_task >> second_task >> sql_writing