import time

current_time = lambda: int(round(time.time()))

print(current_time())

output_file_name = 'df_export'+ str(current_time()) + '.csv'

print(output_file_name)
